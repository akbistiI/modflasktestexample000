import json
import datetime
import time
import pprint
from nose.tools import *

from tests import test_app

def check_content_type_application_json(headers):
  eq_(headers['Content-Type'], 'application/json')

def check_content_type_text_html(headers):
  eq_(headers['Content-Type'], 'text/html; charset=utf-8')

def test_modeltraingtask_routes():
  rv = test_app.get('/modeltrainingtasks')
  check_content_type_application_json(rv.headers)
  resp = json.loads(rv.data)
  #make sure we get a response
  eq_(rv.status_code,200)
  #make sure there are no model training tasks
  eq_(len(resp), 0)

  #create a model training task
  d = dict(sim_task_time=4)
  rv = test_app.post('/modeltrainingtasks', data=d)
  check_content_type_application_json(rv.headers)
  eq_(rv.status_code,202)

 
  #Verify we sent the right data back
  resp = json.loads(rv.data)
  eq_(resp["sim_task_time"],4)
  saveCreatedAt = resp["created_at"] 
  createAgo = datetime.datetime.utcnow() - datetime.datetime.strptime(resp["created_at"],'%Y-%m-%d %H:%M:%S.%f')
  assert createAgo.seconds < 2
 
  #Get model training tasks again...should have one
  rv = test_app.get('/modeltrainingtasks')
  check_content_type_application_json(rv.headers)
  resp = json.loads(rv.data)
  #make sure we get a response
  eq_(rv.status_code,200)
  eq_(len(resp), 1)
 
  #GET the model training task with specified ID
  id = resp[0]['id']
  rv = test_app.get('/modeltrainingtasks/%s' % resp[0]['id'])
  check_content_type_application_json(rv.headers)
  eq_(rv.status_code,200)
  resp = json.loads(rv.data)
  eq_(resp["created_at"],saveCreatedAt)
  eq_(resp["sim_task_time"],4)

  #GET the model training task with specified ID
  time.sleep(2)
  rv = test_app.get('/modeltrainingtasks/%s' % id)
  check_content_type_application_json(rv.headers)
  eq_(rv.status_code,200)
  resp = json.loads(rv.data)
  eq_(resp["created_at"],saveCreatedAt)
  eq_(resp["sim_task_time"],4)

  #GET the model training task with specified ID
  time.sleep(3)
  rv = test_app.get('/modeltrainingtasks/%s' % id)
  check_content_type_text_html(rv.headers)
  eq_(rv.status_code,303)
  eq_(rv.location,'http://www.example.com/model/:id')

# no Unique constraint in model trainging task, should return fine
# Try and add Duplicate model traing task data
  rv = test_app.post('/modeltrainingtasks', data=d)
  check_content_type_application_json(rv.headers)
  eq_(rv.status_code,202)
