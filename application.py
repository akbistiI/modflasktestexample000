import os
import pprint
import datetime
from flask import Flask, redirect
from flask.ext.restful import Resource, reqparse, Api, fields, marshal_with
from sqlalchemy import create_engine, Column, String, Integer, DateTime
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.exc import IntegrityError

app = Flask("flasktestexample")
api = Api(app)
app.debug = True

if os.environ.get('DATABASE_URL') is None:
  engine = create_engine('postgres://modflaskexample:flask@localhost:5432/modflaskexample', convert_unicode=True)
else:
  engine = create_engine(os.environ['DATABASE_URL'], convert_unicode=True)

db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()

@app.teardown_request
def teardown_request(exception):
    db_session.remove()


def init_db():
    Base.metadata.drop_all(bind=engine)
    Base.metadata.create_all(bind=engine)

#Model Training Task Model
class ModelTrainingTask(Base):
    __tablename__ = 'modeltrainingtask'

    #from http://stackoverflow.com/a/11884806
    def as_dict(self):
      obj_d = {c.name: getattr(self, c.name) for c in self.__table__.columns}
      obj_d["created_at"] = str(obj_d["created_at"])
      return obj_d

    id = Column(Integer, primary_key=True)
    #created_at = Column(String(200))
    created_at = Column(DateTime, default=datetime.datetime.utcnow())
    sim_task_time = Column(Integer)

#Parser arguments that Flask-Restful will check for
parser = reqparse.RequestParser()
parser.add_argument('sim_task_time', type=int, required=True, help="sim_task_time Cannot Be Blank")

class DateTimeAsString(fields.Raw):
  def format(self, value):
    return value

mtt_fields = {
  'id': fields.Integer,
  'created_at': DateTimeAsString(),
  'sim_task_time': fields.Integer
}

#Flask Restful Views
class ModelTrainingTaskView(Resource):
  #@marshal_with(mtt_fields)
  def get(self, id):
    e = ModelTrainingTask.query.filter(ModelTrainingTask.id == id).first()
    if e is not None:
      resp = e.as_dict()
      createAgo = datetime.datetime.utcnow() - datetime.datetime.strptime(resp["created_at"],'%Y-%m-%d %H:%M:%S.%f')
      resp["running_time"] = createAgo.seconds
      if createAgo.seconds < resp["sim_task_time"]:
        return resp, 200
      else:
        return redirect("http://www.example.com/model/:id", code=303) #TODO model and id
    else:
      return {}, 200


class ModelTrainingTaskViewList(Resource):
  #@marshal_with(mtt_fields)
  def get(self):
    results = []
    for row in ModelTrainingTask.query.all():
      results.append(row.as_dict())
    return results

  def post(self):
    args = parser.parse_args()
    o = ModelTrainingTask()
    #MRA o.created_at = args["created_at"]
    o.sim_task_time = args["sim_task_time"]

    try:
      db_session.add(o)
      db_session.commit()
    except IntegrityError, exc:
      return {"error": exc.message}, 500

    return o.as_dict(), 202

#Flask Restful Routes
api.add_resource(ModelTrainingTaskViewList, '/modeltrainingtasks')
api.add_resource(ModelTrainingTaskView,     '/modeltrainingtasks/<string:id>')

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
